package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {
  private String makeCompanyesData(List<String> companies) {
    return companies
        .stream()
        .collect(Collectors.joining(System.lineSeparator()));
  }

  private List<String> filterBy(List<String> companies, String by) {
    return companies.stream()
        .filter(company -> company.contains(by))
        .collect(Collectors.toList());
  }

  @Override
  public void doGet(HttpServletRequest request,
      HttpServletResponse response)
      throws IOException, ServletException {

    // BEGIN
    final PrintWriter pw = response.getWriter();
    final String search = request.getParameter("search");
    final List<String> companies = getCompanies();
    if (search == null || search == "") {
      pw.println(this.makeCompanyesData(companies));
      pw.close();
      return;
    }
    final List<String> filtredCompanies = this.filterBy(companies, search);
    if (filtredCompanies.size() == 0) {
      pw.println("Companies not found");
    } else {
      pw.println(this.makeCompanyesData(filtredCompanies));
    }
    pw.close();
    // END
  }
}
