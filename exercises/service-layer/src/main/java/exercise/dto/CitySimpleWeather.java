package exercise.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CitySimpleWeather {
    private String name;
    private String temperature;

    public CitySimpleWeather(CityWeather weather) {
        this.name = weather.getName();
        this.temperature = weather.getTemperature();
    }
}
