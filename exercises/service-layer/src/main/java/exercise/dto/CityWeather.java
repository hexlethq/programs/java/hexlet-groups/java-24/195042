package exercise.dto;

import lombok.Data;

@Data
public class CityWeather {
    private String name;
    private String temperature;
    private String cloudy;
    private String humidity;
    private String wind;
}
