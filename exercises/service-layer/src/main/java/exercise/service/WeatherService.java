package exercise.service;

import exercise.HttpClient;
import exercise.dto.CitySimpleWeather;
import exercise.dto.CityWeather;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import exercise.CityNotFoundException;
import exercise.repository.CityRepository;
import exercise.model.City;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class WeatherService {

    @Autowired
    CityRepository cityRepository;

    // Клиент
    HttpClient client;

    // При создании класса сервиса клиент передаётся снаружи
    // В теории это позволит заменить клиент без изменения самого сервиса
    WeatherService(HttpClient client) {
        this.client = client;
    }

    // BEGIN
    private City getCity(long id) {
        return this.cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City not found"));
    }

    private List<City> getAllOrdered() {
        return this.cityRepository.findAllByOrderByNameAsc();
    }

    private List<City> findByName(String name) {
        return this.cityRepository.findByNameStartingWithIgnoreCase(name);
    }

    private CityWeather getWeather(City city) {
        String url = String.format("http://weather/api/v2/cities/%s", city.getName());
        String data = this.client.get(url);
        ObjectMapper om = new ObjectMapper();
        try {
            return om.readValue(data, CityWeather.class);

        } catch (JsonProcessingException e) {
            throw new RuntimeException();
        }

    }

    public CityWeather getCityWeather(long id) {
        City city = this.getCity(id);
        return this.getWeather(city);
    }

    public List<CitySimpleWeather> getAllCitiesWeather(Optional<String> name) {
        List<City> cities = name
                .map(this::findByName)
                .orElse(this.getAllOrdered());
        return cities.stream()
                .map(this::getWeather)
                .map(CitySimpleWeather::new)
                .collect(Collectors.toList());
    }
    // END
}
