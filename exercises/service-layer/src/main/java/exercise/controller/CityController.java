package exercise.controller;

import exercise.dto.CitySimpleWeather;
import exercise.dto.CityWeather;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@RestController
public class CityController {

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "cities/{id}")
    public CityWeather getCityWeather(@PathVariable("id")
    long id) {
        return weatherService.getCityWeather(id);
    }

    @GetMapping(path = "search")
    public List<CitySimpleWeather> findByName(@RequestParam("name")
    Optional<String> name) {
        return this.weatherService.getAllCitiesWeather(name);
    }
    // END
}
