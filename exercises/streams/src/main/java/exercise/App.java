package exercise;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

// BEGIN
class App {
  public static int getCountOfFreeEmails(List<String> emails) {
    final List<String> freeHosts = List.of("gmail.com", "yandex.ru", "hotmail.com");
    final Predicate<String> getMailHost = mail -> {
      final String host = mail.split("@")[1];
      return freeHosts.contains(host);
    };
    final int freeEmailsCount = (int) Optional.of(emails)
        .orElse(List.of())
        .stream()
        .filter(getMailHost).count();
    return freeEmailsCount;
  }
}
// END
