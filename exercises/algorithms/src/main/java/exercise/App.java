package exercise;

import java.util.Arrays;

class App {
  // BEGIN
  public static int[] sort(int[] numbers) {
    boolean moov = true;
    int lastIdx = numbers.length;

    while (moov) {
      moov = false;
      for (int i = 1; i < lastIdx; i++) {
        int first = numbers[i - 1];
        int last = numbers[i];
        if (first > last) {
          numbers[i - 1] = last;
          numbers[i] = first;
          moov = true;
        }
      }
      lastIdx -= 1;
    }
    return numbers;
  }

  private static int getMinValueIdx(int[] numbers, int firstIdx) {
    int min = Integer.MAX_VALUE;
    int minIdx = firstIdx;
    for (int i = firstIdx; i < numbers.length; i++) {
      if (numbers[i] < min) {
        min = numbers[i];
        minIdx = i;
      }
    }
    return minIdx;
  }

  public static int[] sortSelection(int[] numbers) {
    int minIdx = 0;
    int sortedIdx = 0;
    while (sortedIdx < numbers.length) {
      minIdx = App.getMinValueIdx(numbers, sortedIdx);
      int min = numbers[minIdx];
      int current = numbers[sortedIdx];
      numbers[minIdx] = current;
      numbers[sortedIdx] = min;
      sortedIdx += 1;
    }
    return numbers;
  }
  // END
}
