package exercise;

class App {
  private static boolean checkEven(int number) {
    return number % 2 == 0;
  }

  public static boolean isBigOdd(int number) {
    // BEGIN
    final int max = 1001;
    final boolean isEvan = App.checkEven(number);
    final boolean isBigestOrEqualMax = number >= max;
    final boolean isBigOddVariable = !isEvan && isBigestOrEqualMax;
    // END
    return isBigOddVariable;
  }

  public static void sayEvenOrNot(int number) {
    // BEGIN
    final boolean isEven = App.checkEven(number);
    final String result = isEven ? "yes" : "no";
    System.out.println(result);
    // END
  }

  public static void printPartOfHour(int minutes) {
    // BEGIN
    final int segmentLength = 15;
    final int segment = minutes / segmentLength + 1;

    switch (segment) {
      case 1:
        System.out.println("First");
        break;
      case 2:
        System.out.println("Second");
        break;
      case 3:
        System.out.println("Third");
        break;
      case 4:
        System.out.println("Fourth");
        break;

      default:
        throw new Error("Number nit in range");
    }
    // END
  }
}
