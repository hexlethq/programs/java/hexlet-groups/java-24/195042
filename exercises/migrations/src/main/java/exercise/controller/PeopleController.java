package exercise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/people")
public class PeopleController {
    @Autowired
    JdbcTemplate jdbc;

    @PostMapping(path = "")
    public void createPerson(@RequestBody
    Map<String, Object> person) {
        String query = "INSERT INTO person (first_name, last_name) VALUES (?, ?)";
        jdbc.update(query, person.get("first_name"), person.get("last_name"));
    }

    // BEGIN
    @GetMapping(path = "")
    //@ResponseBody
    public ResponseEntity<List<Map<String, Object>>> findAll() {
        String query = "SELECT * FROM person";
        List<Map<String, Object>> persons = jdbc.queryForList(query);
        return new ResponseEntity<>(persons, HttpStatus.OK);
    }

    @GetMapping(path = "{id}")
    @ResponseBody
    public Map<String, Object> read(@PathVariable("id")
    String id) {
        String query = "SELECT * FROM person WHERE id = ?";
        Map<String, Object> person = jdbc.queryForMap(query, id);
        //return new ResponseEntity<>(person, HttpStatus.OK);
        return person;
    }
    // END
}
