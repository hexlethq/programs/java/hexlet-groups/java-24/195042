package exercise;

import java.nio.file.Path;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

// BEGIN
class App {
  public static void save(Path path, Car car) throws IOException {
    final String json = car.serialize();
    Files.write(path, json.getBytes(), StandardOpenOption.WRITE);
  }

  public static Car extract(Path path) throws IOException {
    final String json = Files.readString(path);
    return Car.unserialize(json);
  }
}
// END
