package exercise;

import lombok.Value;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

// BEGIN
@Value
// END
class Car {
  int id;
  String brand;
  String model;
  String color;
  User owner;

  // BEGIN
  public String serialize() throws JsonProcessingException {
    final ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(this);
  }

  public static Car unserialize(String json) throws IOException {
    final ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.readValue(json, Car.class);
  }
  // END
}
