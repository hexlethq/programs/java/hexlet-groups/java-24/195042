package exercise;

class Point {
  // BEGIN
  private int x;
  private int y;

  public void setY(int y) {
    this.y = y;
  }

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public static Point makePoint(int x, int y) {
    return new Point(x, y);
  }

  public static int getX(Point point) {
    return point.x;
  }

  public static int getY(Point point) {
    return point.y;
  }

  public static String pointToString(Point point) {
    return "(" + point.x + ", " + point.y + ")";
  }

  public static int getQuadrant(Point point) {
    final boolean isXPositive = point.x > 0;
    final boolean isYPositive = point.y > 0;

    if (point.x == 0 || point.y == 0) {
      return 0;
    }

    if (isXPositive && isYPositive) {
      return 1;
    }

    if (!isXPositive && isYPositive) {
      return 2;
    }

    if (!isXPositive && !isYPositive) {
      return 3;
    }

    return 4;
  }

  public static Point getSymmetricalPointByX(Point point) {
    point.setY(point.y * -1);
    return point;
  }

  public static double calculateDistance(Point pointA, Point pointB) {
    return Math.sqrt(Math.pow((pointB.x - pointA.x), 2) + Math.pow((pointB.y - pointA.y), 2));
  }
}
