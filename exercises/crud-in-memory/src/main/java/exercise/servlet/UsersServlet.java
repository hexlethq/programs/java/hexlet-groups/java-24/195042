package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;

import static exercise.Data.getUsers;
import static exercise.Data.getNextId;

public class UsersServlet extends HttpServlet {

    private List<Map<String, String>> users = getUsers();

    private String getId(HttpServletRequest request) {
        return request.getParameter("id");
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, "");
    }

    private Map<String, String> getUserById(String id) {
        Map<String, String> user = users
                .stream()
                .filter(u -> u.get("id").equals(id))
                .findAny()
                .orElse(null);

        return user;
    }

    @Override
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list" :
                showUsers(request, response);
                break;
            case "new" :
                newUser(request, response);
                break;
            case "edit" :
                editUser(request, response);
                break;
            case "show" :
                showUser(request, response);
                break;
            case "delete" :
                deleteUser(request, response);
                break;
            default :
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "new" :
                createUser(request, response);
                break;
            case "edit" :
                updateUser(request, response);
                break;
            case "delete" :
                destroyUser(request, response);
                break;
            default :
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void showUsers(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        request.setAttribute("users", users);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/users.jsp");
        requestDispatcher.forward(request, response);
    }

    private void showUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        String id = getId(request);

        Map<String, String> user = getUserById(id);

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        request.setAttribute("user", user);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/show.jsp");
        requestDispatcher.forward(request, response);
    }

    private void newUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/new.jsp");
        requestDispatcher.forward(request, response);
        // END
    }

    private boolean validateUser(Map<String, String> user) {
        final boolean isValidFirstName = !Optional.ofNullable(user.get("firstName"))
                .orElse("")
                .isBlank();
        final boolean isValidLastName = !Optional.ofNullable(user.get("lastName"))
                .orElse("")
                .isBlank();
        return isValidLastName && isValidFirstName;
    }

    private int unprocessableEntityError = 422;

    private void createUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        final String firstName = request.getParameter("firstName");
        final String lastName = request.getParameter("lastName");
        final String email = request.getParameter("email");

        final String id = getNextId();
        final Map<String, String> user = Map.of(
                "id", id,
                "firstName", firstName,
                "lastName", lastName,
                "email", email);
        if (!validateUser(user)) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/edit.jsp");
            request.setAttribute("error", "FirstName or lastName is empty");
            request.setAttribute("user", user);
            response.setStatus(unprocessableEntityError);
            requestDispatcher.forward(request, response);
            return;
        }
        users.add(user);
        response.sendRedirect("/users");
        // END
    }

    private void editUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        String id = getId(request);

        Map<String, String> user = getUserById(id);

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        // BEGIN
        request.setAttribute("user", user);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/edit.jsp");
        requestDispatcher.forward(request, response);
        // END
    }

    private void updateUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {
        String id = getId(request);

        Map<String, String> user = getUserById(id);

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        // BEGIN
        final String firstName = request.getParameter("firstName");
        final String lastName = request.getParameter("lastName");
        final String email = request.getParameter("email");

        final Map<String, String> updatedUser = Map.of(
                "id", id,
                "firstName", firstName,
                "lastName", lastName,
                "email", email);
        if (!validateUser(updatedUser)) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/edit.jsp");
            request.setAttribute("error", "FirstName or lastName is empty");
            request.setAttribute("user", updatedUser);
            response.setStatus(unprocessableEntityError);
            requestDispatcher.forward(request, response);
            return;
        }
        users.remove(user);
        users.add(updatedUser);
        response.sendRedirect("/users/show?id=" + id);
        // END
    }

    private void deleteUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        String id = getId(request);

        Map<String, String> user = getUserById(id);

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        request.setAttribute("user", user);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/delete.jsp");
        requestDispatcher.forward(request, response);

    }

    private void destroyUser(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        String id = getId(request);

        Map<String, String> user = getUserById(id);

        if (user == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        users.remove(user);
        response.sendRedirect("/users");
    }
}
