package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class App {
  public static Map<String, Integer> getWordCount(String str) {
    final String[] words = str.length() == 0 ? new String[0] : str.split(" ");
    final Map<String, Integer> wordsCount = new HashMap<String, Integer>();
    for (String word : words) {
      final int count = wordsCount.containsKey(word) ? wordsCount.get(word) + 1 : 1;
      wordsCount.put(word, count);
    }
    return wordsCount;
  }

  public static String toString(Map<String, Integer> wordsCount) {
    if (wordsCount.size() == 0) {
      return "{}";
    }
    final int tabSize = 2;
    final String endString = "\n";
    final String seperator = ": ";
    final String tab = " ".repeat(tabSize);
    final StringBuilder strBuilder = new StringBuilder();
    strBuilder.append("{" + endString);
    for (Map.Entry<String, Integer> wordCount : wordsCount.entrySet()) {
      strBuilder.append(tab + wordCount.getKey() + seperator + wordCount.getValue() + endString);
    }
    strBuilder.append("}");
    return strBuilder.toString();
  }
}
// END
