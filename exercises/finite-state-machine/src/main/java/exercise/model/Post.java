package exercise.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.GeneratedValue;

import java.util.Map;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.GenerationType;

@Getter
@Setter
@Entity
@Table(name = "posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;

    @Lob
    private String body;

    private PostState state = PostState.CREATED;

    // BEGIN
    @Transient
    private Map<PostState, Set<PostState>> transitions = Map.of(
            PostState.CREATED, Set.of(PostState.ARCHIVED, PostState.PUBLISHED),
            PostState.ARCHIVED, Set.of(),
            PostState.PUBLISHED, Set.of(PostState.ARCHIVED));

    private boolean transit(PostState toState) {
        boolean canTransit = transitions
                .get(state)
                .contains(toState);
        if (canTransit) {
            state = toState;
        }
        return canTransit;
    }

    public boolean publish() {
        return transit(PostState.PUBLISHED);
    }

    public boolean archive() {
        return transit(PostState.ARCHIVED);
    }
    // END
}
