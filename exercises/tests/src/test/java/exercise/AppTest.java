package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AppTest {

  @Test
  void testTake() {
    // BEGIN
    final int count = 3;
    final List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
    final List<Integer> numbers2 = new ArrayList<>();
    final List<Integer> tobeResult1 = numbers1.stream().limit(count).collect(Collectors.toList());
    final List<Integer> result1 = App.take(numbers1, count);
    final List<Integer> result2 = App.take(numbers2, count);
    final List<Integer> result3 = App.take(numbers1, numbers1.size() + count);
    assertThat(result1).isEqualTo(tobeResult1);
    assertThat(result2).isEqualTo(numbers2);
    assertThat(result3).isEqualTo(numbers1);
    // END
  }
}
