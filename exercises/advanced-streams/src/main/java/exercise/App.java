package exercise;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Arrays;
import java.util.function.Function;

// BEGIN
class App {
  public static String getForwardedVariables(String config) {
    final String envPrefix = "environment=";
    final String forwardedPrefix = "X_FORWARDED_";
    final Function<String, Stream<String>> envParser = str -> {
      final String trimedStr = str
          .replaceAll(envPrefix, "")
          .replaceAll("\"", "");

      return Arrays.stream(trimedStr.split(","))
          .filter(variable -> variable.startsWith(forwardedPrefix))
          .map(variable -> variable.replaceAll(forwardedPrefix, ""));
    };

    final String variables = Arrays.stream(config.split("\n"))
        .filter(str -> str.startsWith(envPrefix))
        .flatMap(envParser)
        .collect(Collectors.joining(","));
    return variables;
  }
}
//END
