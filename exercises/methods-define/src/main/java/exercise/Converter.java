package exercise;

class Converter {
  // BEGIN
  public static int convert(int number, String type) {
    final int kbLength = 1024;

    switch (type) {
      case "b":
        return number * kbLength;
      case "Kb":
        return number / kbLength;
      default:
        return 0;
    }
  }

  public static void main(String[] args) {
    final int kBytes = 10;
    final int bytes = Converter.convert(10, "b");
    System.out.println(kBytes + " Kb" + " = " + bytes + " b");
  }
  // END
}
