package exercise;

class Triangle {
  // BEGIN
  public static double getSquare(int a, int b, int deg) {
    final double rad = deg * Math.PI / 180;
    return 0.5 * a * b * Math.sin(rad);
  }

  public static void main(String[] args) {
    final double square = Triangle.getSquare(4, 5, 45);
    System.out.println(square);
  }
  // END
}
