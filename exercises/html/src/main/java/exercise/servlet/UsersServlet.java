package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.nio.file.Paths;
import java.nio.file.Files;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request,
      HttpServletResponse response)
      throws IOException, ServletException {

    String pathInfo = request.getPathInfo();

    if (pathInfo == null) {
      showUsers(request, response);
      return;
    }

    String[] pathParts = pathInfo.split("/");
    String id = ArrayUtils.get(pathParts, 1, "");

    showUser(request, response, id);
  }

  private List<Map<String, String>> getUsers() throws JsonProcessingException, IOException {
    // BEGIN
    final String usersData = Files.readString(Paths.get(this.getClass().getClassLoader().getResource("users.json").getFile()));
    final ObjectMapper om = new ObjectMapper();
    final List<Map<String, String>> users = om.readValue(usersData, new TypeReference<List<Map<String, String>>>() {
    });
    return users;
    // END
  }

  private String renderUser(Map<String, String> user) {
    final String firstName = user.get("firstName");
    final String lastName = user.get("lastName");
    final String id = user.get("id");
    final String email = user.get("email");
    final String pattern = """
        <tr>
        <td>%s</td>
        <td>
        <a href=\"/users/%s\">%s %s</a>
        </td>
        <td>%s</td>
        </tr>
        """;
    return String.format(pattern, id, id, firstName, lastName, email);
  }

  private String head = """
      <html><body>
      <table>
      """;
  private String tail = """
      </table>
      </body></html>
      """;

  private void showUsers(HttpServletRequest request,
      HttpServletResponse response)
      throws IOException {

    // BEGIN
    final String content = this.getUsers().stream()
        .map(this::renderUser)
        .collect(Collectors.joining("", head, tail));
    response.setContentType("text/html;charset=UTF-8");
    final PrintWriter out = response.getWriter();
    out.println(content);
    // END
  }

  private void showUser(HttpServletRequest request,
      HttpServletResponse response,
      String id)
      throws IOException {

    // BEGIN
    final List<Map<String, String>> users = this.getUsers().stream()
        .filter(user -> user.get("id").equals(id))
        .collect(Collectors.toList());
    if (users.size() == 0) {
      response.sendError(HttpServletResponse.SC_NOT_FOUND);
      return;
    }
    final String content = users.stream()
        .map(this::renderUser)
        .collect(Collectors.joining("", head, tail));
    response.setContentType("text/html;charset=UTF-8");
    final PrintWriter out = response.getWriter();
    out.println(content);

    // END
  }
}
