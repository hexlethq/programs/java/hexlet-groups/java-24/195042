package exercise;

import java.util.Map;
import java.util.Optional;

// BEGIN
class FileKV implements KeyValueStorage {
  private String filePath;

  private void writeDataToFile(Map<String, String> data) {
    final String content = Utils.serialize(data);
    Utils.writeFile(filePath, content);
  }

  private Map<String, String> readDatFromFile() {
    final String content = Utils.readFile(this.filePath);
    return Utils.unserialize(content);
  }

  public FileKV(Map<String, String> data, String filePath) {
    this.filePath = filePath;
    this.writeDataToFile(data);
  }

  @Override
  public void set(String key, String value) {
    final Map<String, String> data = this.readDatFromFile();
    data.put(key, value);
    this.writeDataToFile(data);
  }

  @Override
  public void unset(String key) {
    final Map<String, String> data = this.readDatFromFile();
    data.remove(key);
    this.writeDataToFile(data);
  }

  @Override
  public String get(String key, String defaultValue) {
    final Map<String, String> data = this.readDatFromFile();
    return Optional.ofNullable(data.get(key))
        .orElse(defaultValue);
  }

  @Override
  public Map<String, String> toMap() {
    final Map<String, String> data = this.readDatFromFile();
    return data;
  }
}

// END
