package exercise;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

// BEGIN
class App {
  public static void swapKeyValue(KeyValueStorage storage) {
    final Set<Map.Entry<String, String>> data = storage.toMap().entrySet();
    data.stream()
        .forEach(el -> {
          final String key = el.getKey();
          final String value = el.getValue();
          storage.unset(key);
          storage.set(value, key);
        });
  }
}
// END
