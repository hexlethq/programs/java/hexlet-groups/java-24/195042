package exercise;

import java.util.Map;
import java.util.Optional;
import java.util.HashMap;

// BEGIN
class InMemoryKV implements KeyValueStorage {
  private Map<String, String> collection;

  public InMemoryKV(Map<String, String> collection) {
    this.collection = new HashMap<>(collection);
  }

  @Override
  public void set(String key, String value) {
    this.collection.put(key, value);
  }

  @Override
  public void unset(String key) {
    this.collection.remove(key);
  }

  @Override
  public String get(String key, String defaultValue) {
    return Optional.ofNullable(this.collection.get(key))
        .orElse(defaultValue);
  }

  @Override
  public Map<String, String> toMap() {
    final Map<String, String> copyCollection = new HashMap<>();
    copyCollection.putAll(this.collection);
    return copyCollection;
  }
}
// END
