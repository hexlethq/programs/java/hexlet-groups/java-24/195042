package exercise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import exercise.daytimes.Daytime;

// BEGIN
@RestController
public class WelcomeController {
    @Autowired
    Meal meal;

    @Autowired
    Daytime daytime;

    @GetMapping(path = "/daytime")
    public String welcome() {
        String pattern = "It is %s now. Enjoy your %s";

        return String.format(pattern, daytime.getName(), meal.getMealForDaytime(daytime.getName()));
    }
}
// END
