package exercise;

// BEGIN
class Segment {
  Point a;
  Point b;

  public Segment(Point a, Point b) {
    this.a = a;
    this.b = b;
  }

  public Point getBeginPoint() {
    return a;
  }

  public Point getEndPoint() {
    return b;
  }

  public Point getMidPoint() {
    final int x = (this.a.getX() + this.b.getX()) / 2;
    final int y = (this.a.getY() + this.b.getY()) / 2;

    return new Point(x, y);
  }
}
// END
