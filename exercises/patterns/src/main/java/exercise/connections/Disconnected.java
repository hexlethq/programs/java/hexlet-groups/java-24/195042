package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection {
  private final String name = "disconnected";
  private TcpConnection connection;

  public Disconnected(TcpConnection connection) {
    this.connection = connection;
  }

  @Override
  public void connect() {
    this.connection.setState(new Connected(this.connection));
  }

  @Override
  public void disconnect() {
    System.out.println("Error");
  }

  @Override
  public void write(String data) {
    System.out.println("Error");
  }

  @Override
  public String getStateName() {
    return this.name;
  }
}
// END
