package exercise;

import java.util.List;
import java.util.ArrayList;

import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {
  private String ip;
  private int port;
  private Connection state;
  private List<String> buffer;

  public TcpConnection(String ip, int port) {
    this.ip = ip;
    this.port = port;
    this.state = new Disconnected(this);
    this.buffer = new ArrayList<>();
  }

  public int getPort() {
    return port;
  }

  public String getIp() {
    return ip;
  }

  public void sendData(String data) {
    buffer.add(data);
  }

  public void setState(Connection state) {
    this.state = state;
  }

  public String getCurrentState() {
    return this.state.getStateName();
  }

  public void connect() {
    this.state.connect();
  }

  public void disconnect() {
    this.state.disconnect();
  }

  public void write(String data) {
    this.state.write(data);
  }
}
// END
