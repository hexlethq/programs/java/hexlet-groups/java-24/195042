package exercise;

class App {
  // BEGIN
  static enum Grade {
    ONE(100), TWO(90), THREE(75), DEFAULT(0);

    final private int value;

    private Grade(int value) {
      this.value = value;
    }

    public int getValue() {
      return this.value;
    }
  }

  public static String getTypeOfTriangle(int a, int b, int c) {
    final boolean isExist = a + b > c && b + c > a && a + c > b;
    if (!isExist) {
      return "Треугольник не существует";
    }
    final boolean isEquilateral = a == b && b == c;
    final boolean isIsosceles = a == b || b == c || c == a;
    if (isEquilateral) {
      return "Равносторонний";
    }
    ;
    if (isIsosceles && !isEquilateral) {
      return "Равнобедренный";
    }
    return "Разносторонний";
  }

  public static int getFinalGrade(int exam, int project) {
    final boolean gradeOne = exam > 90 || project > 10;
    if (gradeOne) {
      return Grade.ONE.getValue();
    }
    final boolean gradeTwo = exam > 75 && project >= 5;
    if (gradeTwo) {
      return Grade.TWO.getValue();
    }
    final boolean gradeThree = exam > 50 && project >= 2;
    if (gradeThree) {
      return Grade.THREE.getValue();
    }
    return Grade.DEFAULT.getValue();
  }
  // END
}
