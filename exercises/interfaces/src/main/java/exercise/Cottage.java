package exercise;

import java.util.Comparator;
import java.util.Objects;

// BEGIN
class Cottage implements Home {
  private double area;
  private int floorCount;

  public Cottage(double area, int floorCount) {
    this.area = area;
    this.floorCount = floorCount;
  }

  @Override
  public double getArea() {
    return this.area;
  }

  @Override
  public String toString() {
    return this.floorCount + " этажный коттедж площадью " + this.area + " метров";
  }

  @Override
  public int compareTo(Home another) {
    return Objects.compare(this, another, Comparator.comparingDouble(Home::getArea));
  }
}
// END
