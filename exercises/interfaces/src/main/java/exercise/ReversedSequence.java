package exercise;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// BEGIN
class ReversedSequence implements CharSequence {
  private List<String> reverseList;

  public ReversedSequence(String str) {
    this.reverseList = this.reverse(str);
  }

  private List<String> reverse(String str) {
    List<String> list = Arrays.asList(str.split(""));
    Collections.reverse(list);
    return list;
  }

  @Override
  public String toString() {
    return String.join("", this.reverseList);
  }

  @Override
  public char charAt(int idx) {
    return this.toString().charAt(idx);
  }

  @Override
  public int length() {
    return this.reverseList.size();
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    final String subSequence = String.join("", this.reverseList.subList(start, end));
    return subSequence;
  }
}
// END
