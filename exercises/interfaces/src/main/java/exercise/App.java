package exercise;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class App {
  public static List<String> buildAppartmentsList(List<Home> estatList, int n) {
    return estatList.stream()
        .sorted(Comparator.comparingDouble(Home::getArea))
        .limit(n)
        .map(Home::toString)
        .collect(Collectors.toList());
  }
}
// END
