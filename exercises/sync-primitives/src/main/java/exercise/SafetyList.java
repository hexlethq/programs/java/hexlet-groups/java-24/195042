package exercise;

import java.util.ArrayList;

class SafetyList {
    // BEGIN
    private ArrayList<Integer> list = new ArrayList<>();

    public synchronized void add(int num) {
        this.list.add(num);
    }

    public int get(int idx) {
        return this.list.get(idx);
    }

    public int getSize() {
        return this.list.size();
    }
    // END
}
