package exercise;

import java.util.Random;

// BEGIN
class ListThread extends Thread {
    private static final int MAX_ELEMENTS = 1000;
    private static final int SLEEP_TIME = 1;

    private SafetyList list;
    private Random random = new Random();

    public ListThread(SafetyList list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < MAX_ELEMENTS; i++) {
            try {
                Thread.sleep(SLEEP_TIME);
                this.list.add(this.getRandomNum());
            } catch (InterruptedException e) {
            }
        }
    }

    private int getRandomNum() {
        return this.random.nextInt();
    }
}
// END
