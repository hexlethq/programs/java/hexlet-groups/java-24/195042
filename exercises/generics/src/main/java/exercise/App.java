package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;

// BEGIN
class App {
  private static <T> boolean checkBook(Map<T, T> book, Map<T, T> where) {
    boolean isChecked = true;
    for (Map.Entry<T, T> query : where.entrySet()) {
      if (!isChecked) {
        return isChecked;
      }
      isChecked = book.get(query.getKey()).equals(query.getValue());
    }
    return isChecked;
  }

  public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
    final List<Map<String, String>> result = new ArrayList<>();
    for (Map<String, String> book : books) {
      if (App.checkBook(book, where)) {
        result.add(book);
      }
    }
    return result;
  }
}
// END
