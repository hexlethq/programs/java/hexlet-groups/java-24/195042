package exercise;

import java.util.Arrays;

class App {
  // BEGIN
  public static String getAbbreviation(String phrase) {
    final String[] words = phrase.split(" ");
    final String[] filtredWords = Arrays.stream(words).map(el -> el.trim()).filter(el -> el != "")
        .toArray(String[]::new);
    String abbreviation = "";

    for (int i = 0; i < filtredWords.length; i++) {
      final char firstChar = filtredWords[i].charAt(0);
      abbreviation += firstChar;
    }

    return abbreviation.toUpperCase();
  }
  // END
}
