package exercise;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
class PairedTag extends Tag {
  private String body = "";
  private List<Tag> childs;

  public PairedTag(String name, Map<String, String> attributes, String body, List<Tag> childs) {
    super(name, attributes);
    this.body = body;
    this.childs = childs;
  }

  @Override
  public String toString() {
    final String openTagPart = super.toString();
    final String closeTagPart = "</" + this.getName() + ">";
    final String childTagPart = this.childs
        .stream()
        .map(ch -> ch.toString())
        .collect(Collectors.joining());

    return openTagPart + this.body + childTagPart + closeTagPart;
  }
}
// END
