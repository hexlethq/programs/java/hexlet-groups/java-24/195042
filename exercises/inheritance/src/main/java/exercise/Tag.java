package exercise;

import java.util.stream.Collectors;
import java.util.Map;

// BEGIN
class Tag {
  private String name = "";
  private Map<String, String> attributes;

  public Tag(String name, Map<String, String> attributes) {
    this.name = name;
    this.attributes = attributes;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    final boolean isEmptyAttr = this.attributes == null || this.attributes.size() == 0;
    final boolean isEmptyName = name.isBlank();
    final boolean isEmptyTag = isEmptyAttr || isEmptyName;
    final String head = isEmptyTag ? "<" + this.name : "<" + this.name + " ";
    final String tail = ">";
    return this.attributes
        .entrySet()
        .stream()
        .map(attr -> attr.getKey() + "=\"" + attr.getValue() + "\"")
        .collect(Collectors.joining(" ", head, tail));
  }
}
// END
