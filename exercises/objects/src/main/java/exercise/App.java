package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;

class Li {
  private final String[] li = { "<li>", "</li>" };
  private final int tabLength = 2;
  private final String tab = "\n" + " ".repeat(tabLength);

  private String content;

  public Li(String content) {
    this.content = content;
  }

  public String render() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.tab);
    stringBuilder.append(li[0]);
    stringBuilder.append(this.content);
    stringBuilder.append(li[1]);
    return stringBuilder.toString();

  }
}

class Ul {
  private final String[] ul = { "<ul>", "</ul>" };
  private Li[] lis;

  public Ul(Li[] lis) {
    this.lis = lis;
  }

  public String render() {
    if (this.lis.length == 0) {
      return "";
    }
    StringBuilder stringBuilder = new StringBuilder();
    for (Li li : this.lis) {
      final String liStr = li.render();
      stringBuilder.append(liStr);
    }
    stringBuilder.insert(0, ul[0]);
    stringBuilder.append("\n");
    stringBuilder.append(ul[1]);
    return stringBuilder.toString();
  }
}

class UserComparator implements Comparator<String[]> {
  public int compare(String[] user1, String[] user2) {
    DateTimeFormatter formatterUserData = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    final LocalDate birthday1 = LocalDate.parse(user1[1], formatterUserData);
    final LocalDate birthday2 = LocalDate.parse(user2[1], formatterUserData);
    return birthday2.compareTo(birthday1);

  }
}

class App {
  // BEGIN

  public static String buildList(String[] elements) {
    final Li[] lis = Arrays.stream(elements).map(el -> new Li(el)).toArray(Li[]::new);
    final Ul ul = new Ul(lis);
    return ul.render();
  }

  public static String getUsersByYear(String[][] users, int year) {
    final Li[] usersNamesByYearLi = Arrays.stream(users).filter((user) -> {
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      final LocalDate birthday = LocalDate.parse(user[1], formatter);
      return birthday.getYear() == year;
    }).map(user -> new Li(user[0])).toArray(Li[]::new);

    final Ul ul = new Ul(usersNamesByYearLi);
    return ul.render();
  }
  // END

  // Это дополнительная задача, которая выполняется по желанию.
  public static String getYoungestUser(String[][] users, String date) throws Exception {
    // BEGIN
    DateTimeFormatter formatterUserData = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MM yyyy");
    LocalDate beforeDate = LocalDate.parse(date, formatter);
    String[][] yangestUsers = Arrays.stream(users).filter(user -> {
      final LocalDate birthday = LocalDate.parse(user[1], formatterUserData);
      return birthday.compareTo(beforeDate) < 0;
    }).sorted(new UserComparator()).toArray(String[][]::new);

    return yangestUsers.length > 0 ? yangestUsers[0][0] : "";
    // END
  }
}
