package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;

import java.util.Map;
import java.util.Optional;

import static exercise.App.getUsers;
import exercise.Users;

public class SessionServlet extends HttpServlet {

    private Users users = getUsers();

    @Override
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        if (request.getRequestURI().equals("/login")) {
            showLoginPage(request, response);
            return;
        }

        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Override
    public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        switch (request.getRequestURI()) {
            case "/login" :
                login(request, response);
                break;
            case "/logout" :
                logout(request, response);
                break;
            default :
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void showLoginPage(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
        requestDispatcher.forward(request, response);
    }

    private boolean validateUserAuthData(Map<String, String> user, String email, String password) {
        return user.containsKey("email")
                && user.get("email").equals(email)
                && user.get("password").equals(password);

    }

    private void login(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        final String email = request.getParameter("email");
        final String password = request.getParameter("password");
        final Map<String, String> user = Optional
                .ofNullable(users.findByEmail(email))
                .orElse(users.build());
        final HttpSession session = request.getSession();

        if (!validateUserAuthData(user, email, password)) {
            response.setStatus(422);
            session.setAttribute("flash", "Неверные логин или пароль");
            request.setAttribute("user", user);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/login.jsp");
            requestDispatcher.forward(request, response);
            return;
        }
        session.setAttribute("userId", user.get("id"));
        session.setAttribute("flash", "Вы успешно вошли");
        response.sendRedirect("/");
        // END
    }

    private void logout(HttpServletRequest request,
            HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        final HttpSession session = request.getSession();
        session.removeAttribute("userId");
        session.setAttribute("flash", "Вы успешно вышли");
        response.sendRedirect("/");
        // END
    }
}
