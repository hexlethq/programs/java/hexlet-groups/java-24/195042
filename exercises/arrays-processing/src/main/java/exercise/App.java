package exercise;

import java.util.Arrays;

class App {
  // BEGIN
  public static int getIndexOfMaxNegative(int[] numbers) {
    int idx = -1;
    int maxNegative = Integer.MIN_VALUE;
    for (int i = 0; i < numbers.length; i++) {
      final int value = numbers[i];
      if (value > maxNegative && value < 0) {
        idx = i;
        maxNegative = value;
      }
    }
    return idx;
  }

  public static int[] getElementsLessAverage(int[] numbers) {
    if (numbers.length == 0) {
      return numbers;
    }
    final int avg = Arrays.stream(numbers).sum() / numbers.length;
    final int[] result = Arrays.stream(numbers).filter(value -> value <= avg).toArray();
    return result;
  }

  private static int getIndexArrayElement(int[] numbers, int value) {
    for (int i = 0; i < numbers.length; i++) {
      if (numbers[i] == value) {
        return i;
      }
    }
    return -1;
  }

  public static int getSumBeforeMinAndMax(int[] numbers) {
    final int min = Arrays.stream(numbers).min().getAsInt();
    final int max = Arrays.stream(numbers).max().getAsInt();
    final int minIdx = getIndexArrayElement(numbers, min);
    final int maxIdx = getIndexArrayElement(numbers, max);
    final boolean isMaxIdxBigest = maxIdx > minIdx;
    final int[] beforeMinMax = isMaxIdxBigest ? Arrays.copyOfRange(numbers, minIdx + 1, maxIdx)
        : Arrays.copyOfRange(numbers, maxIdx + 1, minIdx);
    final int sum = Arrays.stream(beforeMinMax).sum();
    return sum;
  }
  // END
}
