package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

// BEGIN
class App {
  private static Set<String> getDicKeys(Map<String, Object> dic) {
    return dic.entrySet().stream().map(en -> en.getKey()).collect(Collectors.toCollection(TreeSet::new));
  }

  private static void compare(String key, Map<String, Object> dicOne, Map<String, Object> dicTwo, Map<String, String> diff) {
    if (!dicOne.containsKey(key) && dicTwo.containsKey(key)) {
      diff.put(key, "added");
    } else if (dicOne.containsKey(key) && !dicTwo.containsKey(key)) {
      diff.put(key, "deleted");
    } else if (!dicOne.get(key).equals(dicTwo.get(key))) {
      diff.put(key, "changed");
    } else {
      diff.put(key, "unchanged");
    }
  }

  public static Map<String, String> genDiff(Map<String, Object> dicOne, Map<String, Object> dicTwo) {
    final Set<String> keys = App.getDicKeys(dicOne);
    keys.addAll(App.getDicKeys(dicTwo));
    final Map<String, String> diff = new LinkedHashMap<>();
    keys.stream().forEach(key -> App.compare(key, dicOne, dicTwo, diff));
    return diff;
  }
}
//END
