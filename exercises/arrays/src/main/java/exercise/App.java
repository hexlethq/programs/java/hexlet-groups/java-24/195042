package exercise;

class App {
  // BEGIN
  public static int[] reverse(int[] src) {
    int[] dest = new int[src.length];
    if (src.length == 0) {
      return dest;
    }
    int i = src.length - 1;
    int j = 0;

    do {
      dest[j] = src[i];
      i--;
      j++;
    } while (i >= 0);

    return dest;
  }

  public static int mult(int[] numbers) {
    int result = 1;

    for (int element : numbers) {
      result *= element;
    }

    return result;
  }

  private static int getMatrixLength(int[][] matrix) {
    int matrixLength = 0;
    for (int[] is : matrix) {
      matrixLength += is.length;
    }
    return matrixLength;
  }

  public static int[] flattenMatrix(int[][] matrix) {
    final int matrixLength = App.getMatrixLength(matrix);
    int[] flatten = new int[matrixLength];
    int idx = 0;
    for (int[] is : matrix) {
      for (int val : is) {
        flatten[idx] = val;
        idx++;
      }
    }
    return flatten;
  }
  // END
}
