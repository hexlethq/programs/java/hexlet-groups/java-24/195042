package exercise;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedHashMap;
// BEGIN
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

class Validator {
  private static String getNotNullInvalid(Field field, Object instance) {
    if (!field.isAnnotationPresent(NotNull.class)) {
      return null;
    }
    try {
      field.setAccessible(true);
      return (String) field.get(instance) == null ? field.getName() : null;

    } catch (IllegalAccessException e) {
      return null;
    }
  }

  private static String getMinLengthInvalid(Field field, Object instance) {
    MinLength minLength = field.getAnnotation(MinLength.class);
    if (minLength == null) {
      return null;
    }
    try {
      field.setAccessible(true);
      return String.valueOf(field.get(instance)).length() < minLength.minLength()
          ? field.getName()
          : null;

    } catch (IllegalAccessException e) {
      return null;
    }
  }

  public static List<String> validate(Object instance) {
    final Field[] fields = instance.getClass().getDeclaredFields();
    return Arrays.stream(fields)
        .map(field -> getNotNullInvalid(field, instance))
        .filter(fieldName -> fieldName != null)
        .collect(Collectors.toList());
  }

  public static Map<String, List<String>> advanceValidate(Object instance) {
    final Field[] fields = instance.getClass().getDeclaredFields();

    final List<String> nullFields = Arrays.stream(fields)
        .map(field -> getNotNullInvalid(field, instance))
        .filter(fieldName -> fieldName != null)
        .collect(Collectors.toList());

    final List<String> minFields = Arrays.stream(fields)
        .map(field -> getMinLengthInvalid(field, instance))
        .filter(fieldName -> fieldName != null)
        .collect(Collectors.toList());

    Set<String> keys = new TreeSet<>(Set.copyOf(nullFields));
    keys.addAll(minFields);

    return keys.stream()
        .map(key -> {
          final List<String> messages = new ArrayList<>();
          if (nullFields.contains(key)) {
            messages.add("Null error");
          }
          if (minFields.contains(key)) {
            messages.add("Min error");
          }
          return Map.of(key, messages);
        })
        .flatMap(map -> map.entrySet().stream())
        .collect(LinkedHashMap::new, (map, field) -> map.put(field.getKey(), field.getValue()), Map::putAll);
  }
}
// END
