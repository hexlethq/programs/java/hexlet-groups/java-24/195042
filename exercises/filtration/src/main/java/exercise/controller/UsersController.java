package exercise.controller;

import exercise.model.User;
//import exercise.model.QUser;
import exercise.repository.UserRepository;

import com.querydsl.core.types.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;

// Зависимости для самостоятельной работы
// import org.springframework.data.querydsl.binding.QuerydslPredicate;
// import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    // BEGIN
    //@GetMapping
    //public Iterable<User> findUsers(@RequestParam(value = "firstName", required = false)
    //String firstName, @RequestParam(value = "lastName", required = false)
    //String lastName) {
    //var firstNameCriteria = firstName == null ? QUser.user.firstName.isNotNull() : QUser.user.firstName.containsIgnoreCase(firstName);
    //var lastNameCriteria = lastName == null ? QUser.user.lastName.isNotNull() : QUser.user.lastName.containsIgnoreCase(lastName);
    //var users = userRepository.findAll(firstNameCriteria.and(lastNameCriteria));

    //return users;
    //}

    @GetMapping
    public Iterable<User> findUsers(@QuerydslPredicate(root = User.class)
    Predicate predicate) {
        return userRepository.findAll(predicate);
    }
    // END
}
