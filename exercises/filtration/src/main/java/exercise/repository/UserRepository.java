package exercise.repository;

import exercise.model.User;
// import org.springframework.data.repository.CrudRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.util.Optional;

import com.querydsl.core.types.dsl.StringPath;
import exercise.model.QUser;

import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends
        JpaRepository<User, Long>,
        QuerydslPredicateExecutor<User>,
        QuerydslBinderCustomizer<QUser> {

    @Override
    default void customize(QuerydslBindings bindings, QUser user) {
        // Дополнительная задача

        // BEGIN
        bindings.bind(user.firstName)
                .firstOptional((path, value) -> Optional.of(path.containsIgnoreCase(value.get())));
        bindings.bind(user.lastName)
                .firstOptional((path, value) -> Optional.of(path.containsIgnoreCase(value.get())));
        bindings.bind(user.email)
                .firstOptional((path, value) -> Optional.of(path.containsIgnoreCase(value.get())));
        bindings.bind(user.profession)
                .firstOptional((path, value) -> Optional.of(path.containsIgnoreCase(value.get())));
        bindings.bind(user.gender)
                .firstOptional((path, value) -> Optional.of(path.eq(value.get())));
        // END
    }

}
