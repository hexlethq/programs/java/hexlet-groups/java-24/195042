package exercise;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

// BEGIN
class App {
  public static boolean scrabble(String symbols, String word) {
    // List.of and Arrays.asList retrun imutable list
    final List<String> symbolList = new ArrayList<String>(Arrays.asList(symbols.split("")));
    final List<String> wordList = Arrays.asList(word.toLowerCase().split(""));
    for (String ch : wordList) {
      if (!symbolList.remove(ch)) {
        return false;
      }
    }
    return true;
  }
}
// END
