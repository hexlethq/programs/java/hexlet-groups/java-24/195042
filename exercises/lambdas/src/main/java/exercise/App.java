package exercise;

import java.util.function.Function;
//import java.util.function.IntFunction;
import java.util.stream.Stream;

// BEGIN
class App {
  //private static <T> T mapToArrayString(T src, IntFunction<T[]> dest, Function<T, Stream<T>> fn) {
  //return Stream.of(src)
  //.flatMap(fn)
  //.toArray(dest);
  //}
  public static String[][] enlargeArrayImage(String[][] image) {
    final Function<String, Stream<String>> mapperElement = element -> Stream.of(new String[] {element, element});
    final Function<String[], Stream<String[]>> mapperRow = row -> {
      final String[] newRow = Stream.of(row)
          .flatMap(mapperElement)
          .toArray(String[]::new);
      return Stream.of(newRow, newRow);
    };
    final String[][] result = Stream.of(image)
        .flatMap(mapperRow)
        .toArray(String[][]::new);
    return result;
  }
}
// END
