package exercise;

import java.util.ArrayList;
import java.util.Arrays;

// BEGIN
class Kennel {
  private static ArrayList<String[]> pets = new ArrayList<String[]>();

  public static void addPuppy(String[] pet) {
    Kennel.pets.add(pet);
  }

  public static void addSomePuppies(String[][] pets) {
    for (String[] pet : pets) {
      Kennel.addPuppy(pet);
    }
  }

  public static int getPuppyCount() {
    return Kennel.pets.size();
  }

  private static String[][] findPuppys(String name) {
    return Kennel.pets.stream().filter(p -> name.equals(p[0])).toArray(String[][]::new);
  }

  public static boolean isContainPuppy(String name) {
    final String[][] pets = Kennel.findPuppys(name);
    return pets.length > 0;
  }

  public static String[][] getAllPuppies() {
    return Kennel.pets.toArray(String[][]::new);
  }

  public static String[] getNamesByBreed(String breed) {
    return Kennel.pets.stream().filter(pet -> breed.equals(pet[1])).map(pet -> pet[0]).toArray(String[]::new);
  }

  public static void resetKennel() {
    Kennel.pets.clear();
  }

  public static boolean removePuppy(String name) {
    final String[][] pets = Kennel.findPuppys(name);
    if (pets.length == 0) {
      return false;
    }
    Kennel.pets.remove(pets[0]);
    return true;
  }
}
// END
