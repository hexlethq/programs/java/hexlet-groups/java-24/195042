// BEGIN
package exercise.geometry;

public class Segment {
  private double[] pointA;
  private double[] pointB;

  public Segment(double[] pointA, double[] pointB) {
    this.pointA = new double[] { pointA[0], pointA[1] };
    this.pointB = new double[] { pointB[0], pointB[1] };
  }

  public double[] getPointA() {
    return pointA;
  }

  public double[] getPointB() {
    return pointB;
  }

  public static Segment makeSegment(double[] pointA, double[] pointB) {
    return new Segment(pointA, pointB);
  }

  public static double[] getBeginPoint(Segment segmant) {
    return segmant.getPointA();
  }

  public static double[] getEndPoint(Segment segment) {
    return segment.getPointB();
  }
}
// END
