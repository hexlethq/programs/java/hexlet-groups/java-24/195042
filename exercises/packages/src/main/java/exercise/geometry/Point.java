// BEGIN
package exercise.geometry;
// END

public class Point {

  public static double[] makePoint(double x, double y) {
    double[] point = { x, y };
    return point;
  }

  public static double getX(double[] point) {
    return point[0];
  }

  public static double getY(double[] point) {
    return point[1];
  }

  public static int getQuadrant(double[] point) {
    final boolean isXPositive = point[0] > 0;
    final boolean isYPositive = point[1] > 0;

    if (point[0] == 0 || point[1] == 0) {
      return 0;
    }

    if (isXPositive && isYPositive) {
      return 1;
    }

    if (!isXPositive && isYPositive) {
      return 2;
    }

    if (!isXPositive && !isYPositive) {
      return 3;
    }

    return 4;
  }

}
