// BEGIN
package exercise;

import exercise.geometry.*;

class App {
  public static double[] getMidpointOfSegment(Segment segment) {
    final double[] pointA = segment.getPointA();
    final double[] pointB = segment.getPointB();
    final double x1 = Point.getX(pointA);
    final double x2 = Point.getX(pointB);
    final double y1 = Point.getY(pointA);
    final double y2 = Point.getY(pointB);
    final double[] middlePoint = Point.makePoint((x1 + x2) / 2, (y1 + y2) / 2);
    return middlePoint;
  }

  public static Segment reverse(Segment segment) {
    final double[] pointA = segment.getPointA();
    final double[] pointB = segment.getPointB();
    return new Segment(pointB, pointA);
  }

  public static boolean isBelongToOneQuadrant(Segment segment) {
    final double[] pointA = segment.getPointA();
    final double[] pointB = segment.getPointB();
    final int pointAQuadrant = Point.getQuadrant(pointA);
    final int pointBQuadrant = Point.getQuadrant(pointB);
    return pointAQuadrant != 0 && pointAQuadrant == pointBQuadrant;
  }
}
// END
