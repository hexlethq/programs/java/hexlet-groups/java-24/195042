package exercise;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private Map<String, String> settings = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (!bean.getClass().isAnnotationPresent(Inspect.class)) {
            return bean;
        }
        Inspect inspect = bean.getClass()
                .getAnnotation(Inspect.class);
        this.settings.put(beanName, inspect.level());
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!settings.containsKey(beanName)) {
            return bean;
        }
        String logLevel = settings.get(beanName);
        String pattern = "Was called method: %s() with arguments: %s";
        Logger logger = LoggerFactory.getLogger(beanName);
        InvocationHandler handler = (proxy, method, args) -> {
            String message = String.format(pattern, method.getName(), Arrays.toString(args));
            switch (logLevel) {
                case "info" :
                    logger.info(message);
                    break;
                case "debug" :
                    logger.debug(message);
                    break;
            }
            return method.invoke(bean, args);
        };

        Object proxyInstance = Proxy.newProxyInstance(
                bean.getClass().getClassLoader(),
                bean.getClass().getInterfaces(),
                handler);

        return proxyInstance;
    }
}
//END
