// BEGIN
package exercise;

import com.google.gson.*;

class App {
  public static void main(String[] args) {
    System.out.println("Hello, World!");
  }

  public static String toJson(String[] src) {
    Gson gson = new Gson();
    String json = gson.toJson(src);
    return json;
  }
}
// END
