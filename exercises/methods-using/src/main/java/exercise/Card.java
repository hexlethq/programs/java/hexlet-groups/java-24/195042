package exercise;

class Card {
  public static void printHiddenCard(String cardNumber, int starsCount) {
    // BEGIN
    final int beginIndex = cardNumber.length() - 4;
    final int endIndex = cardNumber.length();
    final String publicPart = cardNumber.substring(beginIndex, endIndex);
    final String privatePart = "*".repeat(starsCount);

    System.out.println(privatePart + publicPart);
    // END
  }
}
