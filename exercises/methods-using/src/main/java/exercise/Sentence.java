package exercise;

class Sentence {
  public static void printSentence(String sentence) {
    // BEGIN
    final int lastSymbolIndex = sentence.length() - 1;
    final char lastSymbol = sentence.charAt(lastSymbolIndex);
    final String result = lastSymbol == '!' ? sentence.toUpperCase() : sentence.toLowerCase();
    System.out.println(result);
    // END
  }
}
